all:
	mkdir -p build
	ghc -O9 -outputdir build src/Main.hs
zip:
	$(RM) src/Main
	$(RM) src.zip
	zip src.zip -r src

linear:
	/usr/bin/ghc -O9 -outputdir build src/Lin.hs
