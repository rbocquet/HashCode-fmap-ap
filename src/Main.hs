{-# LANGUAGE TupleSections, LambdaCase, ViewPatterns #-}

module Main where

import Data.List
import Data.Maybe
import Data.Char

import Control.Applicative
import Control.Monad

import Control.Lens

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set
import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Data.Vector (Vector)
import qualified Data.Vector as V
import Control.Monad.State
import System.Random
import System.Random.Shuffle
import Control.Monad.Random
import Data.Function (on)
import Data.Traversable
import Data.Foldable

import Debug.Trace
import System.IO

type ServerId = Int
type Row = Int
type Col = Int
type Pool = Int
type Size = Int
type Capacity = Int
type Slots = [({- ROW -} Row, [({- SIZE -} Int, {- SLOT -} Col)])]
type Servers = [(Size, Capacity, ServerId)]

makeSlots :: Int -> Int -> [(Int, Int)] -> Rand StdGen Slots
makeSlots r s unSlots =
  let rows  = Set.fromList (fmap fst unSlots)
      orows = filter (not . (Set.member ?? rows)) [0..]
  in unSlots
     & sort
     & groupBy ((==) `on` fst)
     & fmap (\xs@((x,_):_) -> (x, fmap snd xs))
     & (++ ((\a -> (a, [])) <$> orows))
     & take r
     <&> over _2 ((++ [s]) . ((-1):))
     <&> over _2 sort
     <&> over _2 (\a -> zipWith (\x y -> (x-y-1, y+1)) (tail a) a)
     <&> over _2 (filter ((/=) 0 . fst))
     & traverse (traverse shuffleM)

rendement :: Double -> (Size, Capacity, a) -> Double
rendement n (a, b, _) = - (fromIntegral b) / (fromIntegral a) ** n
 
sortServers :: Double -> [(Int, Int)] -> Rand StdGen Servers
sortServers n servers = zip servers [0..]
                      <&> (\((a, b), c) -> (a, b, c))
                      & sortBy (compare `on` rendement n)
                      & return

type SolveMonad a = StateT (Servers, Map ServerId (Row, Col, Pool, Capacity), Map Pool Capacity, Slots) (Rand StdGen) a

lookupSize :: (Int -> Bool) -> Servers -> Size -> Maybe ((Size, Capacity, ServerId), Servers)
lookupSize i [] _ = Nothing
lookupSize i (m@(a, b, c):as) s
  | a <= s && i a = Just (m, as)
  | otherwise = lookupSize i as s
                <&> (over _2 (m :))

solveSingle :: (Int -> Bool) -> Int -> (Int, Int) -> SolveMonad (Int, Int)
solveSingle i row (size, col) = do
  mp <- use _1
  case lookupSize i mp size of
   Nothing -> return (size, col)
   Just ((sz, cap, sid), sers) -> do
     _1 %= const sers
     pl <- use _3
           <&> Map.toList
           <&> sortBy (compare `on` snd)
           <&> head
           <&> fst
     _3 %= Map.adjust (+ cap) pl
     _2 %= Map.insert sid (row, col, pl, cap)
     solveSingle i row (size - sz, col + sz)

solveSlot :: (Int -> Bool) -> Int -> [(Int, Int)] -> SolveMonad (Int, [(Int, Int)])
solveSlot i row as = do
  traverse (solveSingle i row) as
    <&> (row ,)
                      
solve :: SolveMonad ()
solve = forM_ [const True] $ \i -> do
  p <- use _4
  p' <- traverse (uncurry (solveSlot i)) p
  _4 %= const p'

calcScore :: Pool -> Row -> Map ServerId (Row, Col, Pool, Capacity) -> Int
calcScore p r s = [0..r-1]
                  <&> calcRowScore p s
                  & minimum
  where
    calcRowScore :: Pool -> Map ServerId (Row, Col, Pool, Capacity) -> Row -> Int
    calcRowScore p s r = Map.fold
                         (\(r', _, p, cap) mp -> if r == r' then mp else Map.adjust (+ cap) p mp )
                         (Map.fromList $ zip [0..p-1] (repeat 0))
                         s
                         & Map.toList
                         & fmap snd
                         & minimum


main :: IO ()
main = do
  [r, s, u, p, m] <- fmap read . words <$> getLine
  unSlots <- forM [1..u] $ const $ do
    [r, s] <- fmap read . words <$> getLine :: IO [Int]
    return (r, s)
  servers <- forM [1..m] $ const $ do
    [z, c] <- fmap read . words <$> getLine :: IO [Int]
    return (z, c)
  stdGen <- getStdGen
  let solutions = sortBy (compare `on` view _1) $ evalRand ?? stdGen $ do
        forM [0..15000] $ \i -> do
          slots <- makeSlots r s unSlots
          servers' <- sortServers (0.5 + fromIntegral i / 15000.0) servers
          solution <- view _2 <$> execStateT solve (servers', Map.empty, Map.fromList $ zip [0..p-1] (repeat 0), slots)
          let score = calcScore p r solution
          return (score, 0.5 + fromIntegral i / 15000.0, solution)
  hPutStrLn stderr . show $ view _1 (last solutions)
  hPutStrLn stderr . show $ view _2 (last solutions)
  forM [0..m-1] $ \i -> case Map.lookup i (view _3 (last solutions)) of
    Nothing -> putStrLn "x"
    Just (a, b, c, _) -> putStrLn (unwords . fmap show $ [a, b, c])
  return ()
