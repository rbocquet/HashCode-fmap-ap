module Main where

import Data.List
import Data.Maybe
import Data.Char

import Control.Applicative
import Control.Monad

import Control.Lens

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set
import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Data.Vector (Vector)
import qualified Data.Vector as V
import Control.Monad.State
import Data.Function (on)
import Data.Traversable hiding (forM)
import Data.Foldable hiding (concat, forM_, foldl)

import Debug.Trace
import System.IO

import Data.Algebra
import Data.LinearProgram.Common
import Data.LinearProgram.GLPK.Solver

import Control.Monad

score = (-1, 0, 0)



solve :: Int -> Int -> Int -> Int -> (Int -> Int) -> (Int -> Int) -> (Int -> Int) -> (Int -> Int -> Bool) -> IO (ReturnCode, Maybe (Double, Map (Int, Int, Int) Double))
solve servers blocs groups lignes taille tailleBloc capacite dansLigne =
  let constraints =
        (concat (fmap (\j -> concat (fmap (\k -> fmap (\i -> Constr Nothing (var (i, j, k)) (LBound 0)) [0..servers]) [0..blocs])) [0..groups]))
        ++ (fmap (\i ->
                   Constr Nothing (varSum . concat $ fmap (\j -> fmap (\k -> (i, j, k)) [0..blocs]) [0..groups]) (Bound 0 1)) [0..servers])
        ++ (fmap (\k ->
                   Constr Nothing (linCombination
                                   . concat $ fmap (\j -> (fmap (\i -> (taille i, (i, j, k)))) [0..servers]) [0..groups]) (UBound $ tailleBloc k)) [0..blocs])
        ++ (concat $ fmap (\r ->
                            fmap (\j ->
                                   Constr Nothing (linCombination (((-1), score) : (concat $ fmap (\k -> if dansLigne k r
                                                                                                         then []
                                                                                                         else fmap (\i -> (capacite i, (i, j, k))) [0..servers]) [0..blocs])))                                             (LBound 0)) [0..groups]) [0..lignes])
  in
   let direction = Max in
   let bounds = Map.fromList ((score, LBound 0) : liftM3 (\i j k -> ((i,j,k), Bound 0 1)) [0..servers] [0..groups] [0..blocs]) in
  let types = Map.fromList ((score, IntVar) : liftM3 (\i j k -> ((i,j,k), IntVar)) [0..servers] [0..groups] [0..blocs]) in
  let objective = var score in
  let program = LP direction objective constraints bounds types in
  glpSolveVars mipDefaults program

{--}

type ServerId = Int
type Row = Int
type Col = Int
type Pool = Int
type Size = Int
type Capacity = Int
type Slots = [(Row, Size, Col)]
type Servers = [(Size, Capacity, ServerId)]

makeSlots :: Int -> Int -> [(Int, Int)] -> Slots
makeSlots r s unSlots =
  let rows  = Set.fromList (fmap fst unSlots)
      orows = filter (not . (Set.member ?? rows)) [0..]
  in unSlots
     & sort
     & groupBy ((==) `on` fst)
     & fmap (\xs@((x,_):_) -> (x, fmap snd xs))
     & (++ ((\a -> (a, [-1])) <$> orows))
     & take r
     <&> over _2 (++ [s])
     <&> over _2 sort
     <&> over _2 (\a -> zipWith (\x y -> (x-y-1, y+1)) (tail a) a)
     <&> over _2 (filter ((/=) 0 . fst))
     <&> over _2 sort
     <&> (\(a, as) -> as <&> (\(b, c) -> (a, b, c)))
     & concat


main :: IO ()
main = do
  [r, s, u, p, m] <- fmap read . words <$> getLine
  unSlots <- forM [1..u] $ const $ do
    [r, s] <- fmap read . words <$> getLine :: IO [Int]
    return (r, s)
  servers <- forM [1..m] $ const $ do
    [z, c] <- fmap read . words <$> getLine :: IO [Int]
    return (z, c)
  let vservers = V.fromList servers
  let slots = makeSlots r s unSlots
  let vslots = V.fromList slots
  let ligneDe b = view _1 (vslots V.! b)
  (_, Just (_, solution)) <- solve (s - 1) (length slots - 1) (p - 1) (r - 1)
                 (fst . (vservers V.!))
                 (view _2 . (vslots V.!))
                 (snd . (vservers V.!))
                 (\b r -> view _1 (vslots V.! b) == r)
  let sol = (catMaybes $ map (\((i, j, k), c) -> if i == -1 || c == 0.0 then Nothing else Just (i, j, ligneDe k, k)) (Map.assocs solution))
  let sol' = sol
             & sortBy (compare `on` view _4)
             & groupBy ((==) `on` view _4)
             <&> (\as@((_, _, _, g):_) -> foldl ?? (view _3 $ vslots V.! g, []) ?? as
                                         $ \(col, acc) (srv, _, _, _) -> (col + view _1 (vservers V.! srv), (srv, col):acc))
             <&> snd
             & concat
  let vsol' = V.fromList (snd <$> sol')
  forM_ [0..s-1] $ \i -> case Map.lookup i (Map.fromList $ sol <&> \(a, b, c, d) -> (a, (b, c, d))) of
    Nothing        -> putStrLn "x"
    Just (p, r, _) -> putStrLn $ (unwords . fmap show $ [r, vsol' V.! i, p])
  return ()
